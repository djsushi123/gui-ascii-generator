package com.gitlab.djsushi.view

import javafx.geometry.Pos
import javafx.stage.FileChooser
import tornadofx.*
import java.io.File
import java.util.NoSuchElementException


class FileSelectView : View() {
    override val root = vbox {

        setPrefSize(1000.0, 500.0)

        alignment = Pos.CENTER

        label(
            """
                Hello, user! This is a very simple application that can turn virtually any image into
                an ASCII image. Go ahead, give it a try!
            """.trimIndent().replace("\n", " ")
        )

        button(text = "Choose a file...") {

            action {
                val file: File? = try {
                    chooseFile(title = "Choose a file...",
                        mode = FileChooserMode.Single,
                        filters = arrayOf(FileChooser.ExtensionFilter("Image", "*.png", "*.jpg"))
                    ).first()
                } catch (e: NoSuchElementException) { null }

                println(if (file != null) "Selected file $file" else "No file selected.")

                if (file != null) {
                    replaceWith<ASCIIViewerView>()
                }
            }
        }
    }
}