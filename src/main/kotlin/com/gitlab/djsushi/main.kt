package com.gitlab.djsushi

import tornadofx.launch

fun main() {
    launch<MyApp>()
}