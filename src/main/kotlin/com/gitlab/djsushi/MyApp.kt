package com.gitlab.djsushi

import com.gitlab.djsushi.view.FileSelectView
import javafx.stage.Stage
import tornadofx.App

class MyApp: App(FileSelectView::class, Styles::class) {

    override fun start(stage: Stage) {
        with (stage) {
            minWidth = 300.0
            minHeight = 200.0
            super.start(this)
        }

    }

}